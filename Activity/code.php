<!-- [SECTION  CLASS] PERSON -->


<?php
class Person
{

    // PROPERTIES OBJECT 
    public $firstname;
    public $middlename;
    public $lastname;


    // Construictor
    public function __construct($firstname, $lastname,  $middlename)
    {

        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->middlename = $middlename;
    }

    public function printName()
    {
        return "Your Fulll name is $this->firstname $this->lastname";
    }
};

// // Instantiatng Peron Class 

$person =  new Person("Senku", "Ishigani", "Bercasio");


// [SECTION] Inheritance and Polymorphism



class Developer extends Person
{
    public function printName()
    {
        return "Your name is $this->firstname $this->middlename $this->lastname and you are a developer";
    }
}

$developer = new Developer("Johm", "Finch", "Smith");


class Engineer extends Person
{
    public function printName()
    {
        return "Your name is $this->firstname $this->middlename $this->lastname and you are a developer";
    }
}

$engineer = new Engineer("Harold", "Mmyres", "Reses");
